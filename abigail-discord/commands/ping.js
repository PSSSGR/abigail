module.exports = {
    name: 'ping',
    description: 'Ping to the user',
    execute(message, args) {
        message.channel.send('Pong.');
    },
};
