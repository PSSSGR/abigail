var request = require('request');
var MovieDB = require('moviedb')('a24257b7c5e458997a319bb3222368ac');

const api_location = 'http://localhost:8983/solr/abigail/'
const poster_url = 'https://image.tmdb.org/t/p/w500'

module.exports = {
    name: 'search',
    description: 'Search information from solr db',
    execute(message, args) {
        var temp = args.toString();
        
        /**
         * The string coverted variable is parsed to add '+' for every space
         * for the specific keywords based search from our database.
         * This is to comply with solr's rules for this sort of indexing.
         */

        var search_query = temp.replace(/,/g, "+")
        console.log(search_query);
        var options = {
            //url: api_location + 'select?q=\"upward+tropics\"' FORMAT FOR QUERY 
            url: api_location + `select?q=\"${search_query}\"`
        }

        function callback(error, response, body) {
            if (!error && response.statusCode == 200) {
                file = JSON.parse(body);
                if (file.response.numFound != 0 && file.response.numFound < 10) {
                    //message.channel.send(`You entered ${search_query}`);
                    //message.channel.send(`Number found: ${file.response.numFound}`);


                    for (var i = 0; i < file.response.numFound; i++) {
                        var arr_map = [['movie_name', file.response.docs[i].id]]
                        var map_movie = new Map(arr_map);


                        var movie_name = '';
                        movie_name = map_movie.get('movie_name')
                        console.log(movie_name);

                        /**
                         * Our subtitles are indexed from a path in our unix-like
                         * operating system. Those path are included in our solr's 
                         * index and to remove them we pop from the last '/'. This makes
                         * it not work on Windows without WSL, however a few lines of code
                         * can be added to fix it, for those who want windows server support.
                         */

                        movie_name = movie_name.split("/").pop()
                        
                        console.log(movie_name);
                        console.log(typeof (movie_name));

                        MovieDB.searchMovie({ query: movie_name }, (err, res) => {
                            var arr_res = [['title', res.results[0].title], 
                                          ['id', res.results[0].id],
                                          ['description', res.results[0].overview],
                                          ['poster', res.results[0].poster_path]
                                          ];
                            var map_resources = new Map(arr_res); 

                            console.log(map_resources.get('title'))

                            MovieDB.movieInfo({ id: map_resources.get('id') }, (err, mess) => {
                               //The api gets us more data using id instead of the name.
                                var arr_message = [['id', mess.imdb_id],
                                                  ['release_date', mess.release_date],
                                                  ['budget', money_commas(mess.budget)],
                                                  ['revenue', money_commas(mess.revenue)],
                                                  ['runtime', mess.runtime]
                                                  ];
                                var map_message = new Map(arr_message);
                                console.log(mess);

                                /**
                                 * The format used here is different from standard embed.
                                 * This is called rich embed and it makes the embed more
                                 * pleasant looking for the end user of the bot. This is 
                                 * completly api-agonistic.
                                 */

                                const embed = {
                                    "description": map_resources.get('description'),
                                    "url": "https://discordapp.com",
                                    "color": 3721471,
                                    "image": {
                                        "url": poster_url + map_resources.get('poster')
                                    },
                                    fields: [
                                        {
                                            name: "Release Date: ",
                                            value: map_message.get('release_date'),
                                        },
                                        {
                                            name: "Budget: ",
                                            value: map_message.get('budget') + " USD",
                                        },
                                        {
                                            name: "Revenue: ",
                                            value: map_message.get('revenue') + " USD",
                                        },
                                        {
                                            name: "Runtime: ",
                                            value: map_message.get('runtime') + " Minutes",
                                        },
                                    ],
                                    "author": {
                                        "name": `${map_resources.get('title')}`,
                                        "url": `https://imdb.com/title/${map_message.get('id')}`
                                    }
                                };
                                message.channel.send({ embed });
                            });
                        });
                    }
                }

                else {
                    if (file.response.numFound > 9)
                        message.channel.send("We can't show such generic results.")
                    else {
                        message.channel.send("No result found");
                    }
                }
            }
        }

        request(options, callback);
    },
};

function money_commas(x) {
    /**
     * The function takes a value from user, converts it to a string
     * and for every 3 numbers add a comma. This is used for money values.
     */
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}