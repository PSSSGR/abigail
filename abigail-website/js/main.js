const api_location = 'api_loc';

console.log("connected");

var api = new Map();//Hashmap-datastructure
var movies_found = new Map();//Hashmap-datastructure
var movie_details = new Map();//Hashmap-datastructure


let file;
let titles;
var search_query;

api.set(api_location,'http://localhost:8983/solr/abigail/');//sets the api map to solr api.

$(document).ready(() => {
    $('#search_bar').on('submit', (res) => {//getting the text from the user and refining it.
        search_query = $('#search_query').val();
        search_query = search_query.replace(/ /g, "+");//removing the spaces and adding "+" to parse text properly.
        const url = api.get(api_location)+  `select?q=\"${search_query}\"`;//setting the url to send the request.
        fetch(url)//fetching the json data from the api.
            .then(function(response) {
                console.log(response);
                return response.json();//its comes as string so we convert it to json.
                })
            .then(function(myJson) {
                console.log(myJson);
                callback(myJson);//calls the function.
            });
        res.preventDefault();
    });
});


function callback(body) {
        file = new Map();
        file = body;
        let output = '';
        movie_found = file.response.numFound;
        if (movie_found != 0 && movie_found < 9) {
        
            console.log(`You entered ${search_query}`)
            console.log(`Number found: ${file.response.numFound}`)
            
            for (var i = 0; i < file.response.numFound;  i++){
                console.log(typeof(file.response.docs[i].id));
                var name = file.response.docs[i].id.toString();
                console.log(name);
                var title = name.substring(name.lastIndexOf("/")+1,name.length);
                var final_title = title.replace(/ /g, "+");
                //Refining the title to parse it to api. 
                console.log(title);
                $.getJSON("https://api.themoviedb.org/3/search/movie?api_key=a56477782d138276312376af187168cf&language=en-US&query=" + final_title + "&callback=?",
                //requesting the moviedb api and getting json info.
                function(json) {
                    console.log(json);
                    console.log(json.results[0].title);
                    console.log(json.results[0].overview);
                    console.log(json.results[0].id);
                    var movie_title = "movie_title";
                    var movie_plot = "movie_plot";
                    var movie_id = NaN;
                    var movie_path = "movie_path";
                    movies_found.set(movie_path, json.results[0].poster_path)//setting up the hashmap.
                    movies_found.set(movie_title, json.results[0].title);
                    movies_found.set(movie_plot, json.results[0].overview);
                    movies_found.set(movie_id, json.results[0].id);
                    console.log(movies_found.get(movie_path));
                    //presenting the info to end user through website.
                    output += `
                        <div class="col-md-3">
                            <div class="well text-center">
                            
                            <img src=\"http://image.tmdb.org/t/p/w500/${movies_found.get(movie_path)}\"class=\"img-responsive\" >
                            <h5>${movies_found.get(movie_title)}</h5>
                            <a onclick="get_movie_info('${movies_found.get(movie_id)}')" class="btn btn-primary" href="#">Movie Details</a>
                            </div>
                        </div>
                        `;

                        $('#movies_found').html(output);
                });

            }

        }else{//if no results are found
            if(movie_found == 0){
                output += `
                        <div class="col-md-3">
                            <div class="text-center">
                            <h5>Nothing Found <br> Please Try Again</h5>
                            </div>
                        </div>
                        `;

                        $('#exp').html(output);
            }else{//if it is very generic term. 
                output += `
                        <div class="col-md-3">
                            <div class="text-center">
                            <h5>We cannot search such generic terms.<br> Please Try Again</h5>
                            </div>
                        </div>
                        `;

                        $('#exp').html(output);

            }
            console.log("No result found")
        }
}

function get_movie_info(id){
    sessionStorage.setItem('movieId', id);
    window.location = 'movie.html';
    return false;
}

function Show_movie_info(){
    let movieId = sessionStorage.getItem('movieId');
    //requesting the api for specific movie details.
    axios.get('https://api.themoviedb.org/3/movie/'+movieId+'?api_key=a56477782d138276312376af187168cf&language=en-US')
      .then((response) => {
        console.log(response);
        let movie = response.data;
        var genre = '';
        var production_companies = '';

        $.each(movie.genres, function(index, value){//refining the movie generes as they are in array.
            var temp = value.name;
            console.log(temp);
            genre += "<br> " +temp;
            console.log(value.name);
            console.log(genre);
        });

        $.each(movie.production_companies, function(index, value){//refining the production companies as they are in array.
            var temp = value.name;
            console.log(temp);
            production_companies += "<br> " +temp;
            console.log(value.name);
            console.log(production_companies);
        });

        var movie_path = "path";
        var movie_title = "title";
        var movie_genre = "genre";
        var movie_production_companies = "production_companies";
        var movie_homepage = "homepage";
        var movie_tagline = "tagline";
        var movie_release_date = "release_date";
        var movie_budget = "budget";
        var movie_revenue = "revenue";
        var movie_runtime = "runtime";
        var movie_plot = "plot";
        var movie_imdbid = "imdb";



        movie_details.set(movie_path, movie.poster_path);//setting up the data-structure with movie-details.
        movie_details.set(movie_title, movie.title);
        movie_details.set(movie_genre, genre);
        movie_details.set(movie_production_companies,production_companies);
        movie_details.set(movie_homepage, movie.homepage);
        movie_details.set(movie_tagline, movie.tagline);
        movie_details.set(movie_release_date, movie.release_date);
        movie_details.set(movie_budget, movie.budget);
        movie_details.set(movie_revenue, movie.revenue);
        movie_details.set(movie_runtime, movie.runtime);
        movie_details.set(movie_plot, movie.overview);
        movie_details.set(movie_imdbid, movie.imdb_id);

        console.log(production_companies);
        //presenting the output of the movie to the user.
        let output =`
          <div class="row">
            <div class="col-md-4">
            <img src=\"http://image.tmdb.org/t/p/w500/${movie_details.get(movie_path)}\"class=\"img-responsive\" >
            </div>
            <div class="col-md-8">
              <h2 class="jumbotron">${movie_details.get(movie_title)}</h2>
              <ul class="list-group">
                <li class="list-group-item"><strong>Genre:</strong> ${movie_details.get(movie_genre)}</li>
                <li class="list-group-item"><strong>Production Companies:</strong> ${movie_details.get(movie_production_companies)}</li>
                <li class="list-group-item"><strong>Movie webpage:</strong><a href="${movie_details.get(movie_homepage)}" target="_blank"> Click here to visit the official Website</a></li>
                <li class="list-group-item"><strong>Movie tagline:</strong> ${movie_details.get(movie_tagline)}</li>
                <li class="list-group-item"><strong>Released:</strong> ${movie_details.get(movie_release_date)}</li>
                <li class="list-group-item"><strong>Budget(in USD): $</strong> ${money_commas(movie_details.get(movie_budget))}</li>
                <li class="list-group-item"><strong>Revenue(in USD): $</strong> ${money_commas(movie_details.get(movie_revenue))}</li>
                <li class="list-group-item"><strong>Runtime(in min):</strong> ${movie_details.get(movie_runtime)}</li>
              </ul>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="well jumbotron">
              <h3>Plot</h3>
              ${movie_details.get(movie_plot)}
              <hr>
              <a href="http://imdb.com/title/${movie_details.get(movie_imdbid)}" target="_blank" class="btn btn-primary">View IMDB</a>
              <a href="index.html" target="_blank" class="btn btn-primary">Go Back To Search</a>
            </div>
          </div>
        `;
  
        $('#movie_selected').html(output);
      })
      .catch((err) => {
        console.log(err);
      });
  }

function money_commas(x) {//to get movie in comma format.
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}